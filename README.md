# Healthaid

Healthaid is a daemon that monitors the health status of the service.

## Architecture

![Architecture](https://cacoo.com/diagrams/G1eKZ34uvkh3rdB2-1577C.png)

## Installation

Add this line to your application's Gemfile:

    gem 'healthaid'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install healthaid

## Usage

### Starting Server

```sh
healthaid start -s 'healthaid/plugin/shell' -c '{"command":"mysqladmin ping -h 127.0.0.1"}'
```

### Health Checking

```
curl localhost:11180
```

### Showing Status

```sh
healthaidctl status
```

```
  healthy: false
  enabled: true
   script: healthaid/plugin/shell
   config: {"command":"mysqladmin ping -h 127.0.0.1"}
 interval: 3
    debug: false
threshold: {"healthy":1,"unhealthy":3}
```
