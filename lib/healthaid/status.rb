module Healthaid
  class Status
    def initialize(server)
      @server  = server
      @health  = true
      @enabled = true

      @healthy_count   = 0
      @unhealthy_count = 0
    end

    def add_healthy
      prev_health = @health

      if @health = ((@unhealthy_count = 0; @healthy_count += 1) >= @server.options.fetch(:healthy))
        @server.logger.info('service is healthy') if @health != prev_health
      end
    end

    def add_unhealthy
      prev_health = @health

      unless @health = !((@healthy_count = 0; @unhealthy_count += 1) >= @server.options.fetch(:unhealthy))
        @server.logger.info('service is unhealthy') if @health != prev_health
      end
    end

    def healthy?
      @health
    end

    def enable
      @enabled = true
    end

    def disable
      @enabled = false
    end

    def enabled?
      @enabled
    end

    def available?
      enabled? and healthy?
    end
  end # Healthaid
end # Status
