require 'mysql2/em'

module Healthaid
module Plugin

class Mysql
  def initialize(status, config, logger)
    @status = status
    @config = config
    @logger = logger
  end

  def check
    timeout = @config.fetch(:timeout, 1).to_i
    @config[:read_timeout]    ||= timeout
    @config[:write_timeout]   ||= timeout
    @config[:connect_timeout] ||= timeout

    client = Mysql2::EM::Client.new(@config)
    defer = client.query(@config.fetch(:query, 'SELECT 1'))

    defer.timeout(timeout)

    defer.callback do
      @status.add_healthy
      client.close
    end

    defer.errback do
      @status.add_unhealthy
      client.close
    end
  end
end # Mysql

end # Plugin
end # Healthaid
