require 'em-http-request'

module Healthaid
module Plugin

class Http
  def initialize(status, config, logger)
    @status = status
    @config = config
    @logger = logger
  end

  def check
    uri = @config.fetch(:uri, 'http://127.0.0.1/')
    ok  = @config[:ok] || [200]

    case ok
    when String
      ok = ok.strip.split(/\s*,\s*/).map {|i| i.to_i }
      ok = [200] if ok.empty?
    when Array
      ok = ok.map {|i| i.to_i }
    else
      ok = [ok.to_i]
    end

    timeout = @config.fetch(:timeout, 1).to_i
    @config[:connect_timeout]    ||= timeout
    @config[:inactivity_timeout] ||= timeout

    http = EM::HttpRequest.new(uri, @config).get
    http.timeout(timeout)

    http.callback do
      if ok.include?(http.response_header.status)
        @status.add_healthy
      else
        @status.add_unhealthy
      end
    end

    http.errback do
      @status.add_unhealthy
    end
  end
end # Http

end # Plugin
end # Healthaid
