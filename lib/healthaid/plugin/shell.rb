module Healthaid
module Plugin

class Shell
  class Handler < EventMachine::Connection
    include EM::Deferrable

    def initialize
      super
      @data = []
    end

    def receive_data data
      @data << data
    end

    def unbind
      if get_status.success?
        succeed(@data.join)
      else
        fail(@data.join)
      end
    end
  end # Handler

  def initialize(status, config, logger)
    @status = status
    @config = config
    @logger = logger
  end

  def check
    timeout = @config.fetch(:timeout, 1).to_i
    command  = @config.fetch(:command)

    defer = EM.popen(command, Handler)

    defer.timeout(timeout)
    defer.callback { @status.add_healthy }
    defer.errback { @status.add_unhealthy }
  end
end # Mysql

end # Plugin
end # Healthaid
