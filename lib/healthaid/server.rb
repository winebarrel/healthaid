require 'drb/drb'
require 'fileutils'
require 'logger'
require 'rexec'
require 'rexec/daemon'
require 'eventmachine'
require 'evma_httpserver'

module Healthaid
  class Server < RExec::Daemon::Base
    class << self
      def options=(options)
        @@options        = options
        @@base_directory = options[:working_dir]
        @@controller     = Healthaid::Controller.new(self)
        @@status         = Healthaid::Status.new(self)
        @@medic          = Healthaid::Medic.new(self)
        @@timer          = nil
        @@logger         = nil
      end

      def options; @@options; end
      def status;  @@status;  end
      def medic;   @@medic;   end
      def timer;   @@timer;   end
      def logger;  @@logger;  end

      def run
        trap(:PIPE, nil)

        @@logger = Logger.new(STDOUT)
        update_loglevel

        start_drb
        run_em
      end

      def reload_script
        @@medic.reset_script
      end

      def update_loglevel
        @@logger.level = @@options[:debug] ? Logger::DEBUG : Logger::INFO
      end

      def update_interval
        interval = @@options.fetch(:interval)
        @@timer.interval = interval
      end

      def remove_socket
        socket = @@options.fetch(:socket)
        FileUtils.rm_f(socket)
      end

      private
      def start_drb
        socket = @@options.fetch(:socket)
        FileUtils.rm_f(socket)
        DRb.start_service("drbunix:#{socket}", @@controller)
        File.chmod(0700, socket)
      end

      def run_em
        addr = @@options.fetch(:addr)
        port = @@options.fetch(:port)
        interval = @@options.fetch(:interval)

        EM.epoll
        EM.threadpool_size = @@options[:threads] if @@options[:threads]

        EM.run {
          EM.start_server(addr, port, Healthaid::HttpServer, self)

          @@timer = EM::PeriodicTimer.new(interval) do
            @@medic.run if @@status.enabled?
          end
        }
      end
    end # self
  end # Server
end # Healthaid

# extend RExec
module RExec
  module Daemon
    class Base
      def self.daemon_name; APP_NAME; end
    end # Base

    module Controller
      class << self
        alias stop_orig stop

        def stop(daemon)
          Healthaid::Server.remove_socket
          stop_orig(daemon)
        end
      end # self
    end # Controller
  end # Daemon
end # RExec
