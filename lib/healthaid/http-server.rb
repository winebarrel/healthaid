require 'eventmachine'
require 'evma_httpserver'

module Healthaid
  class HttpServer < EM::Connection
    include EM::HttpServer

    OK = 200
    NG = 503

    def initialize(server)
      @server = server
    end

    def process_http_request
      res = EM::DelegatedHttpResponse.new(self)
      res.content_type 'text/plain'

      @server.logger.debug("receive request: hash=#{res.hash}")

      EM::defer do
        res.status = @server.status.available? ? OK : NG
        res.content = res.status

        @server.logger.debug("send response: hash=#{res.hash} status=#{res.status}")

        res.send_response
      end
    end
  end # HttpServer
end # Healthaid
