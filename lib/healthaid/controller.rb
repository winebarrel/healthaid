module Healthaid
  class Controller
    def initialize(server)
      @server = server
    end

    def status
      {
        'healthy'   => @server.status.healthy?,
        'enabled'   => @server.status.enabled?,
        'script'    => @server.options[:script],
        'config'    => JSON.dump(@server.options[:config]),
        'interval'  => @server.options[:interval],
        'debug'     => !!@server.options[:debug],
        'threshold' => {
          'healthy'   => @server.options[:healthy],
          'unhealthy' => @server.options[:unhealthy],
        }
      }
    end

    def enable
      @server.status.enable
    end

    def disable
      @server.status.disable
    end

    def reload_script
      @server.reload_script
    end

    def script=(value)
      @server.options[:script] = value
    end

    def config=(value)
      @server.options[:config] = value
    end

    def debug=(value)
      @server.options[:debug] = !!value
      @server.update_loglevel
    end

    def interval=(value)
      @server.options[:interval] = value.to_i
      @server.update_interval
    end

    def healthy=(value)
      @server.options[:healthy] = value.to_i
    end

    def unhealthy=(value)
      @server.options[:unhealthy] = value.to_i
    end
  end
end
