module Healthaid
  class Medic
    def initialize(server)
      @server = server
    end

    def run
      begin
        load_script
        @server.logger.debug("run scipt: #{@server.options[:script]}")
        @script.check
      rescue => e
        @server.logger.error e.message
        @server.logger.debug e.backtrace.join("\n")
        @server.status.add_unhealthy
      end
    end

    def reset_script
      @script = nil
    end

    private
    def load_script
      return @script if @script

      @server.logger.debug("load scipt: #{@server.options[:script]} config=#{JSON.dump @server.options[:config]}")
      script = @server.options.fetch(:script)
      loaded = false

      if File.readable?(script)
        load(script)
        loaded = true
      else
        $:.each {|dir|
          path = File.join(dir, script)

          if File.readable?(path)
            load(path)
            loaded = true
            break
          end

          path += '.rb'

          if File.readable?(path)
            load(path)
            loaded = true
            break
          end
        }
      end

      raise LoadError, "cannot load such file -- #{script}" unless loaded

      @script = create_script(script)
    end

    def create_script(script)
      class_name = File.basename(script, '.rb').split(/[-_]+/).map {|i| i.split(//) }
      class_name = class_name.map {|i| i[0].upcase + i[1..-1].join.downcase }.join
      clazz = Healthaid::Plugin.const_get(class_name)

      config = @server.options.fetch(:config)
      config = symbolize_keys(config)
       Healthaid::Plugin.const_get(class_name).new(@server.status, config, @server.logger)
    end

    def symbolize_keys(src)
      dst = {}

      src.each do |key, value|
        dst[key] = value
        dst[key.to_sym] = value
      end

      return dst
    end
  end # Medic
end # Healthaid
