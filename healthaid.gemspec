# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'healthaid/version'

Gem::Specification.new do |spec|
  spec.name          = "healthaid"
  spec.version       = Healthaid::VERSION
  spec.authors       = ["Genki Sugawara"]
  spec.email         = ["sugawara@cookpad.com"]
  spec.description   = %q{Healthaid is a daemon that monitors the health status of the service.}
  spec.summary       = %q{Healthaid is a daemon that monitors the health status of the service.}
  spec.homepage      = "https://bitbucket.org/winebarrel/healthaid"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "mysql2"

  spec.add_dependency 'rexec', '~> 1.6.0'
  spec.add_dependency 'eventmachine', '~> 1.0.3'
  spec.add_dependency 'eventmachine_httpserver', '~> 0.2.1'
  spec.add_dependency 'em-http-request', '~> 1.1.0'
  spec.add_dependency 'json'
end
